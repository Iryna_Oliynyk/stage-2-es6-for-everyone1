import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
  });
}
function processFightAction(firstFighter, secondFighter, keyMap, currentCode) {
  if (currentCode === controls.PlayerOneBlock) {
    firstFighter.setIsBlocking(true);
  }
  if (currentCode === controls.PlayerTwoBlock) {
    secondFighter.setIsBlocking(true);
  }
  if (currentCode === controls.PlayerOneAttack) {
    applyFighterAttack(firstFighter, secondFighter, keyMap);
    return;
  }
  if (currentCode === controls.PlayerTwoAttack) {
    applyFighterAttack(secondFighter, firstFighter, keyMap);
    return;
  }
  if (controls.PlayerOneCriticalHitCombination.every(code => keyMap.has(code))) {
    firstFighter.doCritAttack(secondFighter);
    return;
  }
  if (controls.PlayerTwoCriticalHitCombination.every(code => keyMap.has(code))) {
    secondFighter.doCritAttack(firstFighter);
  }
}

function applyFighterAttack(attacker, defender) {
  if (attacker.isBlocking) {
    return;
  }

  if (defender.isBlocking) {
    attacker.doAttack(defender, 0);
    return;
  }

  attacker.doAttack(defender, getDamage(attacker, defender));
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodjeChance = Math.random() + 1;
  return fighter.defense * dodjeChance;
}